# Warehouse demo project

This project is a demo of an entry exam for TU Wien's Advanced Software Engineering PR course
in the winter semester of 2019. I have tried my best to keep the API as close to the test as
possible, but there may be slight differences.

## Differences and disclaimer

* The project was written in [Kotlin](https://kotlinlang.org/) using [Gradle](https://gradle.org/) 
as its build system. During the test, the project has to be implemented in Java (11) using
Maven.
* Additional libraries and technologies are freely selectable. For this project, the
[Spring framework](https://spring.io/) was used. Database access was done through JDBC
and Spring's built-in validation library was used to check for errors.
* Currently there is no extensive test suite for this project. A bigger test suite is planned, but
will only resemble the actual test suite used during the test.
* This project is potentially slightly over-engineered. During the test, the only factor that
matters is that the test suite passes.

## Resources
* To create a new Spring project easily, consider using [Spring Initializr](https://start.spring.io/).
* [Building a RESTful Web Service](https://start.spring.io/) is a light introduction that covers some
basic concepts used in this project.
* [Spring in Action](https://www.manning.com/books/spring-in-action-fifth-edition) is a good book that
goes well beyond what is needed for a simple project such as this one.