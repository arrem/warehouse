package at.tuwien.alem.ase.warehouse.db

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import java.sql.Connection
import java.sql.DriverManager

@Repository
class Database {
    @Value("\${spring.datasource.url}")
    private lateinit var dataSource: String
    @Value("\${spring.datasource.username}")
    private lateinit var username: String
    @Value("\${spring.datasource.password}")
    private lateinit var password: String

    val connection: Connection by lazy {
        DriverManager.getConnection("$dataSource?user=$username&password=$password")
    }
}

class DatabaseException(message: String) : RuntimeException(message)