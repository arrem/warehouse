package at.tuwien.alem.ase.warehouse.controller

import at.tuwien.alem.ase.warehouse.db.ProductRepository
import at.tuwien.alem.ase.warehouse.domain.ErrorObject
import at.tuwien.alem.ase.warehouse.domain.Product
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.Errors
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/product")
class ProductController(val productRepository: ProductRepository) {

    @PostMapping
    fun createProduct(@Valid @RequestBody product: Product, errors: Errors): ResponseEntity<*> {
        if (errors.hasErrors()) {
            return ErrorObject(errors).asResponse(HttpStatus.UNPROCESSABLE_ENTITY)
        }

        val response = productRepository.create(product)

        return ResponseEntity.ok(response)
    }

    @GetMapping("/{id}")
    fun getProductById(@PathVariable id: Long): ResponseEntity<*> {
        val product = productRepository.getById(id) ?:
                return ErrorObject("No product with the id $id was found.").asResponse(HttpStatus.NOT_FOUND)

        return ResponseEntity.ok(product)
    }

    @GetMapping
    fun getAllProducts(): ResponseEntity<*> {
        return ResponseEntity.ok(productRepository.getAllProductInfo())
    }
}