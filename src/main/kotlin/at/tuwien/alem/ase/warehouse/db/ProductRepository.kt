package at.tuwien.alem.ase.warehouse.db

import at.tuwien.alem.ase.warehouse.domain.Product
import at.tuwien.alem.ase.warehouse.domain.ProductInfo
import org.springframework.stereotype.Component
import java.sql.Statement

@Component
class ProductRepository(val database: Database) {
    private val createStatement = "INSERT INTO product (name, amount, price, created) VALUES (?, ?, ?, ?);"
    private val getByIdStatement = "SELECT * FROM product WHERE id = ?;"
    private val getAllInfoStatement = "SELECT name, amount FROM product;"

    fun create(product: Product): Product {
        database.connection.prepareStatement(createStatement, Statement.RETURN_GENERATED_KEYS).use { statement ->
            statement.setString(1, product.name)
            statement.setInt(2, product.amount ?: 0)
            statement.setBigDecimal(3, product.price)
            statement.setTimestamp(4, product.created)

            if (statement.executeUpdate() == 0) {
                throw DatabaseException("Could not create product $product.")
            }

            val keys = statement.generatedKeys

            if (!keys.next()) {
                throw DatabaseException("No key was returned for $product.")
            }

            return product.copy(id = keys.getLong(1))
        }
    }

    fun getById(id: Long): Product? {
        database.connection.prepareStatement(getByIdStatement).use { statement ->
            statement.setLong(1, id)

            val result = statement.executeQuery()

            if (!result.next()) {
                return null
            }

            return Product(
                    result.getLong("id"),
                    result.getString("name"),
                    result.getInt("amount"),
                    result.getBigDecimal("price"),
                    result.getTimestamp("created")
            )
        }
    }

    fun getAllProductInfo(): List<ProductInfo> {
        database.connection.prepareStatement(getAllInfoStatement).use { statement ->
            val result = statement.executeQuery()

            val products = mutableListOf<ProductInfo>()

            while (result.next()) {
                products.add(ProductInfo(result.getString("name"), result.getInt("amount")))
            }

            return products
        }
    }
}
