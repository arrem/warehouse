package at.tuwien.alem.ase.warehouse.domain

import java.math.BigDecimal
import java.sql.Timestamp
import java.time.Instant
import javax.validation.constraints.NotNull
import javax.validation.constraints.PositiveOrZero

data class Product(
        val id: Long?,
        @field:NotNull(message = "A product name must be specified.")
        val name: String?,
        @field:[NotNull(message = "Invalid amount.") PositiveOrZero(message = "Invalid amount.")]
        val amount: Int?,
        @field:[NotNull(message = "Invalid price.") PositiveOrZero(message = "Invalid price.")]
        val price: BigDecimal?,
        val created: Timestamp = Timestamp.from(Instant.now())
)

data class ProductInfo(val name: String, val amount: Int)