package at.tuwien.alem.ase.warehouse.domain

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.Errors

class ErrorObject(val errors: List<String>) {
    constructor(errors: Errors) : this(errors.fieldErrors.mapNotNull { it.defaultMessage })
    constructor(error: String) : this(listOf(error))

    fun asResponse(code: HttpStatus) = ResponseEntity(this, code)
}