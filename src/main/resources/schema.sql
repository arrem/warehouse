CREATE TABLE IF NOT EXISTS product (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    amount INT,
    price NUMERIC,
    created TIMESTAMP
);