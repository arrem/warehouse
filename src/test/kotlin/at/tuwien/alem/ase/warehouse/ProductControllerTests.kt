package at.tuwien.alem.ase.warehouse

import at.tuwien.alem.ase.warehouse.controller.ProductController
import at.tuwien.alem.ase.warehouse.db.Database
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@WebMvcTest(ProductController::class)
@ComponentScan
class ProductControllerTests {
    @Autowired
    private lateinit var database: Database
    @Autowired
    private lateinit var mvc: MockMvc

    @Before
    fun database() {
        database.connection.prepareStatement("TRUNCATE TABLE product RESTART IDENTITY;").execute()
    }

    @Test
    fun `create succeeds with correct info`() {
        val content = """{"name":"Name","amount":20,"price":7.5}"""
        val response = """{"id":1,"name":"Name","amount":20,"price":7.5}"""

        mvc.perform(post("/api/v1/product").contentType("application/json").content(content))
                .andExpect(status().isOk)
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json(response))
    }

    @Test
    fun `create fails with null name`() {
        val content = """{"amount":20,"price":7.5}"""

        mvc.perform(post("/api/v1/product").contentType("application/json").content(content))
                .andExpect(status().`is`(HttpStatus.UNPROCESSABLE_ENTITY.value()))
    }

    @Test
    fun `create fails with null or invalid amount`() {
        val content = listOf(
                """{"name":"Name","price":7.5}""",
                """{"name":"Name","amount":-20,"price":7.5}"""
        )

        content.forEach {
            mvc.perform(post("/api/v1/product").contentType("application/json").content(it))
                    .andExpect(status().`is`(HttpStatus.UNPROCESSABLE_ENTITY.value()))
        }
    }

    @Test
    fun `create fails with null or invalid price`() {
        val content = listOf(
                """{"id":1,"name":"Name","amount":20}""",
                """{"id":1,"name":"Name","amount":20,"price":-7.5}"""
        )

        content.forEach {
            mvc.perform(post("/api/v1/product").contentType("application/json").content(it))
                    .andExpect(status().`is`(HttpStatus.UNPROCESSABLE_ENTITY.value()))
        }
    }
}